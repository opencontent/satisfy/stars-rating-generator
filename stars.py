#!/usr/bin/env python
from datetime import datetime, date, timedelta

import json
import os
import random
from hashlib import md5

import requests
import numpy as np

from random import choices

HOST_URI = os.getenv('SATISFY_HOST_URI', default='http://0.0.0.0:8080')
START_DATE = os.getenv('START_DATE', default='2022-01-10')
END_DATE = os.getenv('END_DATE', default='2022-01-18')


def send_rating(sleep_min, sleep_max, rating_value, tenant, entrypoint, title, uri, user_agent, current_date):
    rating_value_weights = list(np.random.uniform(0, 1, 5))

    number_of_ratings = random.randint(80, 120)
    for i in range(number_of_ratings):
        obj = {
            "version": 1,
            "widget-version": "1.3.0",
            "id": f"demo_bot_{md5(bytes(random.randint(1, 10000))).hexdigest()}",
            "value": choices(rating_value, rating_value_weights)[0],
            "timestamp": current_date.isoformat(),
            "entrypoint-id": "db5071c2-bc05-41fc-8ded-5a9e12a9776f",
            "meta": {
                "uri": "https://www.comune.bugliano.pi.it/",
                "title": "Comune di Bugliano",
                "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36",
                "lang": "it-IT"
            },
            "expiry": 1674301953313,
        }
        requests.post(HOST_URI, data=json.dumps(obj), timeout=20)
        # print(obj['tenant-name'], "/", obj['entrypoint-name'], ': Sent', obj['value'])


if __name__ == "__main__":
    rating_value = list(range(1, 6))

    # tenant = os.environ.get('TENANT', 'comune-di-genova')
    tenant = ['comune-di-monopoli', 'comune-di-tavagnacco']

    # entrypoint = os.environ.get('ENTRYPOINT', 'segnalaci')
    entrypoint = ['sdc-www2', 'opencity']

    uri_sdc_monopoli = ['https://www2.stanzadelcittadino.it/comune-di-monopoli/servizi',
                        'https://www2.stanzadelcittadino.it/comune-di-monopoli/pratiche']
    uri_sdc_tavagnacco = ['https://www2.stanzadelcittadino.it/comune-di-tavagnacco/servizi',
                          'https://www2.stanzadelcittadino.it/comune-di-tavagnacco/pratiche']

    uri_opencity_monopoli = ['https://www.comune.monopoli.ba.it/Amministrazione',
                             'https://www.comune.monopoli.ba.it/Novita']
    uri_opencity_tavagnacco = ['https://www.comune.tavagnacco.ud.it/Amministrazione',
                               'https://www.comune.tavagnacco.ud.it/Novita']

    title_sdc = ['Tutti i servizi', 'Le mie pratiche']
    title_opencity = ['Amministrazione', 'Novita']
    user_agent = [
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.3',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/43.4',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
        'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) ',
        'Version/10.0 Mobile/14E304 Safari/602.1'
    ]
    sleep_min = int(os.environ.get('SLEEP_MIN', 1))
    sleep_max = int(os.environ.get('SLEEP_MAX', 3))

    start_date = datetime.strptime(START_DATE, '%Y-%m-%d')
    end_date = datetime.strptime(END_DATE, '%Y-%m-%d')
    delta = timedelta(days=1)

    while start_date <= end_date:
        print(datetime.strptime(str(start_date).split(" ")[0], "%Y-%m-%d"))

        tenant_weights = list(np.random.uniform(0, 1, 2))
        entrypoint_weights = list(np.random.uniform(0, 1, 2))

        selected_tenant = choices(tenant, tenant_weights)[0]
        selected_entrypoint = choices(entrypoint, entrypoint_weights)[0]

        if selected_tenant == 'comune-di-monopoli' and selected_entrypoint == 'sdc-www2':
            send_rating(sleep_min, sleep_max, rating_value, selected_tenant, selected_entrypoint, title_sdc,
                        uri_sdc_monopoli, user_agent, start_date)
        elif selected_tenant == 'comune-di-monopoli' and selected_entrypoint == 'opencity':
            send_rating(sleep_min, sleep_max, rating_value, selected_tenant, selected_entrypoint, title_opencity,
                        uri_opencity_monopoli, user_agent, start_date)
        elif selected_tenant == 'comune-di-tavagnacco' and selected_entrypoint == 'sdc-www2':
            send_rating(sleep_min, sleep_max, rating_value, selected_tenant, selected_entrypoint, title_sdc,
                        uri_sdc_tavagnacco, user_agent, start_date)
        elif selected_tenant == 'comune-di-tavagnacco' and selected_entrypoint == 'opencity':
            send_rating(sleep_min, sleep_max, rating_value, selected_tenant, selected_entrypoint, title_opencity,
                        uri_opencity_tavagnacco, user_agent, start_date)

        start_date += delta
